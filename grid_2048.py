import random as rd
import numpy as np


def create_grid(grid_length=4):
    game_grid = []
    for i in range(grid_length):
        game_grid.append([' ' for j in range(grid_length)])
    return game_grid
def test_create_grid(grid_length=4):
    assert create_grid(grid_length)==[[' ' for j in range(grid_length)] for i in range(grid_length)]


def get_value_new_tile():
    random_value=rd.randint(1,10) #peut prendre 10 valeurs,
                                # si cette valeur est 10 alors on crée une tuile 4 sinon on crée une tuile 2.
                                #De cette façon on a 90% de chance de créer une tuile 2.
    if random_value==10:
        return 4
    return 2
def test_get_value_new_tile():
 assert get_value_new_tile()== 2 or 4


def grid_add_new_tile_at_position(game_grid, i, j):
    new_grid = game_grid
    new_grid[i][j]=get_value_new_tile()
    return new_grid

def test_grid_add_new_tile_at_position():
 game_grid=create_grid(4)
 game_grid=grid_add_new_tile_at_position(game_grid,1,1)
 assert game_grid in [[[' ',' ',' ', ' '],[' ', 2 ,' ', ' '],[' ',' ',' ', ' '],[' ', ' ', ' ', ' ']], [[' ',' ',' ', ' '],[' ', 4 ,' ', ' '],[' ',' ',' ', ' '],[' ', ' ', ' ', ' ']]]


def get_all_tiles(game_grid):
    grid_length=len(game_grid)
    tiles_list=[]
    for i in range(grid_length):
        for j in range(grid_length):
            if game_grid[i][j]==' ':
                tiles_list.append(0)
            else:
                tiles_list.append(game_grid[i][j])
    return tiles_list
def test_get_all_tiles():
 assert get_all_tiles( [[' ',4,8,2], [' ',' ',' ',' '], [' ',512,32,64],
[1024,2048,512, ' ']]) == [0,4,8,2,0,0,0,0,0,512,32,64, 1024,2048,512,0]
 assert get_all_tiles([[16,4,8,2], [2,4,2,128], [4,512,32,64],
[1024,2048,512,2]]) == [16, 4, 8, 2, 2, 4, 2, 128, 4, 512, 32, 64, 1024, 2048, 512,
2]
 assert get_all_tiles(create_grid(3))== [0 for i in range(9)]

def get_empty_tiles_positions(game_grid):
    grid_length=len(game_grid)
    empty_tiles_positions=[]
    for i in range(grid_length):
        for j in range(grid_length):
            if game_grid[i][j]==' ' or game_grid[i][j]==0:
                empty_tiles_positions.append((i,j))
    return empty_tiles_positions
def test_get_empty_tiles_positions():
 assert get_empty_tiles_positions([[0, 16, 32, 0], [64, 0, 32, 2], [2, 2, 8, 4],
[512, 8, 16, 0]])==[(0,0),(0,3),(1,1),(3,3)]
 assert get_empty_tiles_positions([[' ', 16, 32, 0], [64, 0, 32, 2], [2, 2, 8,
4], [512, 8, 16, 0]])==[(0,0),(0,3),(1,1),(3,3)]
 assert get_empty_tiles_positions(create_grid(2))==[(0,0),(0,1),(1,0),(1,1)]
 assert get_empty_tiles_positions([[16,4,8,2], [2,4,2,128], [4,512,32,64],
[1024,2048,512,2]])==[]


def grid_get_value(game_grid, x, y):
    if game_grid[x][y]==' ':
        return 0
    else:
        return game_grid[x][y]
def get_new_position(game_grid):
    empty_tiles_positions=get_empty_tiles_positions(game_grid)
    number_of_empty_tiles=len(empty_tiles_positions)
    random = rd.randint(0,number_of_empty_tiles-1)
    random_empty_position = empty_tiles_positions[random]
    return random_empty_position #ici on veut renvoyer la position d'une case vide, mais on veux que cette case soit choisie aléatoirement parmi les cases vides de la grille de jeu...
def test_get_new_position():
 grid = [[0, 16, 32, 0], [64, 0, 32, 2], [2, 2, 8, 4], [512, 8, 16, 0]]
 x,y=get_new_position(grid)
 assert(grid_get_value(grid,x,y)) == 0
 grid = [[' ',4,8,2], [' ',' ',' ',' '], [' ',512,32,64], [1024,2048,512, ' ']]
 x,y=get_new_position(grid)
 assert(grid_get_value(grid,x,y)) == 0


def grid_add_new_tile(game_grid):
    new_position=get_new_position(game_grid)
    column, line = new_position
    return grid_add_new_tile_at_position(game_grid, column, line)
def test_grid_add_new_tile():
 game_grid=create_grid(4)
 game_grid=grid_add_new_tile(game_grid)
 tiles = get_all_tiles(game_grid)
 assert 2 or 4 in tiles

def init_game(grid_length=4):
    game_grid=create_grid(grid_length)
    game_grid=grid_add_new_tile(game_grid)
    game_grid=grid_add_new_tile(game_grid)
    return game_grid
def test_init_game():
 grid = init_game(4)
 tiles = get_all_tiles(grid)
 assert 2 or 4 in tiles
 assert len(get_empty_tiles_positions(grid)) == 14



def grid_to_string(game_grid, grid_length):
    string_to_print=grid_length*" ==="
    string_to_print+="\n" #passage à la ligne
    for i in range(grid_length):
        for j in range(grid_length):
            if game_grid[i][j]==' ':
                string_to_print+="|   "
            else:
                string_to_print+="| %s " %(game_grid[i][j])
        string_to_print+="| \n"
        string_to_print+=grid_length*" ==="
        string_to_print+="\n"
    return string_to_print
def test_grid_to_string():
 a ="""
 === === === ===
|   |   |   |   |
 === === === ===
|   |   |   |   |
 === === === ===
|   |   |   |   |
 === === === ===
| 2 |   |   | 2 |
 === === === ===
 """
 return grid_to_string([[' ', ' ', ' ', ' '], [' ', ' ', ' ', ' '], [' ', ' ', ' ', ' '], [2, ' ', ' ', 2]],4)==a


def get_bigger(list):
    bigger=list[0]
    for i in range(1,len(list)):
        if list[i]>bigger:
            bigger=list[i]
    return bigger
def long_value(game_grid):
    return len(str(get_bigger(get_all_tiles(game_grid))))

def grid_to_string_with_size(game_grid, grid_length):
    size_tile=long_value(game_grid)+2 #même pour la plus grosse chaîne de caractère, on veut garder un espace à droite et à gauche dans la case
    string_to_print=grid_length*(" " +size_tile*"=") #bordure haute
    string_to_print+="\n" #passage à la ligne
    for i in range(grid_length):
        for j in range(grid_length):
            if game_grid[i][j]==' ':
                string_to_print+=("|"+size_tile*" ")
            else:
                space_left=(size_tile-len(str(game_grid[i][j])))//2 #pour garder les éléments centrés dans les tuiles, on définit l'espace qu'il faut mettre à droite et à gauche
                space_right=size_tile-len(str(game_grid[i][j])) - space_left
                string_to_print+="|"+space_left*" "+ "%s" %(game_grid[i][j]) + space_right*" "
        string_to_print+="| \n"
        string_to_print+=grid_length*(" " +size_tile*"=")
        string_to_print+="\n"
    return string_to_print


THEMES = {"0": {"name": "Default", 0: "", 2: "2", 4: "4", 8: "8", 16: "16", 32:
"32", 64: "64", 128: "128", 256: "256", 512: "512", 1024: "1024", 2048: "2048",
4096: "4096", 8192: "8192"}, "1": {"name": "Chemistry", 0: "", 2: "H", 4: "He", 8:
"Li", 16: "Be", 32: "B", 64: "C", 128: "N", 256: "O", 512: "F", 1024: "Ne", 2048:
"Na", 4096: "Mg", 8192: "Al"}, "2": {"name": "Alphabet", 0: "", 2: "A", 4: "B", 8:
"C", 16: "D", 32: "E", 64: "F", 128: "G", 256: "H", 512: "I", 1024: "J", 2048: "K",
4096: "L", 8192: "M"}}

def long_value_with_theme(game_grid, theme):
    bigger_number=get_bigger(get_all_tiles(game_grid))
    return len(theme[bigger_number])
def test_long_value_with_theme():
 grid =[[2048, 16, 32, 0], [0, 4, 0, 2], [0, 0, 0, 32], [512, 1024, 0, 2]]
 assert long_value_with_theme(grid,THEMES["0"]) == 4
 assert long_value_with_theme(grid,THEMES["1"]) == 2
 assert long_value_with_theme(grid,THEMES["2"]) == 1
 grid = [[16, 4, 8, 2], [2, 4, 2, 128], [4, 512, 32, 4096], [1024, 2048, 512,
2]]
 assert long_value_with_theme(grid,THEMES["0"]) == 4
 assert long_value_with_theme(grid,THEMES["1"]) == 2
 assert long_value_with_theme(grid,THEMES["2"]) == 1

def grid_to_sting_with_size_and_theme(game_grid, theme, grid_length):
    size_tile=long_value_with_theme(game_grid, theme)+2 #même pour la plus grosse chaîne de caractère, on veut garder un espace à droite et à gauche dans la case
    string_to_print=grid_length*(" " +size_tile*"=") #bordure haute
    string_to_print+="\n" #passage à la ligne
    for i in range(grid_length):
        for j in range(grid_length):
            if game_grid[i][j]=="":
                string_to_print+=("|"+size_tile*" ")
            else:
                space_left=(size_tile-len(theme[game_grid[i][j]]))//2 #pour garder les éléments centrés dans les tuiles, on définit l'espace qu'il faut mettre à droite et à gauche
                space_right=size_tile-len(theme[game_grid[i][j]]) - space_left
                string_to_print+="|"+space_left*" "+ theme[game_grid[i][j]] + space_right*" "
        string_to_print+="| \n"
        string_to_print+=grid_length*(" " +size_tile*"=")
        string_to_print+="\n"
    return string_to_print
def test_grid_to_string_with_size_and_theme():
 grid=[[16, 4, 8, 2], [2, 4, 2, 128], [4, 512, 32, 64], [1024, 2048, 512, 2]]
 a="""
 ==== ==== ==== ====
| Be | He | Li | H  | 
 ==== ==== ==== ====
| H  | He | H  | N  | 
 ==== ==== ==== ====
| He | F  | B  | C  | 
 ==== ==== ==== ====
| Ne | Na | F  | H  | 
 ==== ==== ==== ====
"""
 assert grid_to_sting_with_size_and_theme(grid,THEMES["1"],4)== a[1:]

def move_row_left(row):
    new_row=[]
    is_already_fusionned=[]
    for i in range(len(row)):
        if row[i]!=0:
            if len(new_row)>0 and row[i]==new_row[-1] and is_already_fusionned[-1]==0:
                new_row[-1]*=2
                is_already_fusionned[-1]=1
            else:
                new_row.append(row[i])
                is_already_fusionned.append(0)
    new_row+=(len(row)-len(new_row))*[0]
    return new_row
def test_move_row_left():
 assert move_row_left([0, 0, 0, 2]) == [2, 0, 0, 0]
 assert move_row_left([0, 2, 0, 4]) == [2, 4, 0, 0]
 assert move_row_left([2, 2, 0, 4]) == [4, 4, 0, 0]
 assert move_row_left([2, 2, 2, 2]) == [4, 4, 0, 0]
 assert move_row_left([4, 2, 0, 2]) == [4, 4, 0, 0]
 assert move_row_left([2, 0, 0, 2]) == [4, 0, 0, 0]
 assert move_row_left([2, 4, 2, 2]) == [2, 4, 4, 0]
 assert move_row_left([2, 4, 4, 0]) == [2, 8, 0, 0]
 assert move_row_left([4, 8, 16, 32]) == [4, 8, 16, 32]
def move_row_right(row):
    new_row=[]
    is_already_fusionned=[]
    for i in range(len(row)-1,-1,-1):
        if row[i]!=0:
            if len(new_row)>0 and row[i]==new_row[0] and is_already_fusionned[0]==0:
                new_row[0]*=2
                is_already_fusionned[0]=1
            else:
                new_row=[row[i]]+new_row
                is_already_fusionned=[0]+is_already_fusionned
    new_row=(len(row)-len(new_row))*[0]+new_row
    return new_row

def test_move_row_right():
 assert move_row_right([2, 0, 0, 0]) == [0, 0, 0, 2]
 assert move_row_right([0, 2, 0, 4]) == [0, 0, 2, 4]
 assert move_row_right([2, 2, 0, 4]) == [0, 0, 4, 4]
 assert move_row_right([2, 2, 2, 2]) == [0, 0, 4, 4]
 assert move_row_right([4, 2, 0, 2]) == [0, 0, 4, 4]
 assert move_row_right([2, 0, 0, 2]) == [0, 0, 0, 4]
 assert move_row_right([2, 4, 2, 2]) == [0, 2, 4, 4]
 assert move_row_right([2, 4, 4, 0]) == [0, 0, 2, 8]
 assert move_row_right([4, 8, 16, 32]) == [4, 8, 16, 32]
def transpose(tableau):rint(transpose([[1,2,3],[4,5,6],[7,8,9]]))
