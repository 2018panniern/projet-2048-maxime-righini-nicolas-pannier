def read_player_command():
 move = input("Entrez votre commande (g (gauche), d (droite), h (haut), b(bas)):")
 if move in ['g', 'd', 'h', 'b']:
    return move
 else:
     print("il faut entrer une commande valide!")
     return read_player_command()

def read_size_grid():
   taille=input("Avec quelle taille de grille voulez-vous jouer?")
   try:
       int(taille)
       if int(taille)<=0:
           print("Il faut demander un entier strictement positif!")
           return(read_size_grid())
       else:
        return taille
   except ValueError:
       print("Il faut entrer un entier strictement positif!")
       return(read_size_grid())

def read_theme_grid():
 theme = input("Entrez le theme que vous desirez (Default, Chemistry ou Alphabet):")
 if theme in ['Default', 'Chemistry', 'Alphabet']:
    return theme
 else:
    print("il faut entrer un theme valide!")
    return read_theme_grid()



